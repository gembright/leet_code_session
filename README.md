# leet_code_session
practice leetcode, a 3-months study group

# rules
1. English only
2. hangout style 
3. google document
4. bi-weekly
5. one question for each member
6. each member has 20 mins at most  
7. welcome to invite others

# code
1. prefer talking than typing when describing problem
2. no need to paste any constrain in the begining
3. no ide support

# events
* ~~20200509 - 10:30 - 11:30 am~~
* ~~20200523 - 10:30 - 11:30 am~~
* ~~20200606 - 10:30 - 11:30 am~~  Delay: 20200614 - 9:30 - 11:00 
*   ~~20200614 - 09:30 - 11:00 am~~
* ~~20200621 - 09:30 - 11:00 am~~
* 20200705 - 09:30 - 11:00 am
* 20200719 - 09:30 - 11:00 am

# members
|  | note |
| ------ | ------ |
| gem | host |
| allen |  |
| roger |  |
| brian | |
